/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateHeroInput = {
  id?: string | null,
  image: string,
  active: boolean,
  altImage?: string | null,
  page?: string | null,
};

export type ModelHeroConditionInput = {
  image?: ModelStringInput | null,
  active?: ModelBooleanInput | null,
  altImage?: ModelStringInput | null,
  page?: ModelStringInput | null,
  and?: Array< ModelHeroConditionInput | null > | null,
  or?: Array< ModelHeroConditionInput | null > | null,
  not?: ModelHeroConditionInput | null,
};

export type ModelStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}


export type ModelSizeInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type ModelBooleanInput = {
  ne?: boolean | null,
  eq?: boolean | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type UpdateHeroInput = {
  id: string,
  image?: string | null,
  active?: boolean | null,
  altImage?: string | null,
  page?: string | null,
};

export type DeleteHeroInput = {
  id?: string | null,
};

export type CreateCategoryInput = {
  id?: string | null,
  name: string,
  image: string,
  slug: string,
};

export type ModelCategoryConditionInput = {
  name?: ModelStringInput | null,
  image?: ModelStringInput | null,
  slug?: ModelStringInput | null,
  and?: Array< ModelCategoryConditionInput | null > | null,
  or?: Array< ModelCategoryConditionInput | null > | null,
  not?: ModelCategoryConditionInput | null,
};

export type UpdateCategoryInput = {
  id: string,
  name?: string | null,
  image?: string | null,
  slug?: string | null,
};

export type DeleteCategoryInput = {
  id?: string | null,
};

export type CreateProductInput = {
  id?: string | null,
  name: string,
  image: string,
  price: number,
  slug: string,
  productCategoryId?: string | null,
};

export type ModelProductConditionInput = {
  name?: ModelStringInput | null,
  image?: ModelStringInput | null,
  price?: ModelFloatInput | null,
  slug?: ModelStringInput | null,
  and?: Array< ModelProductConditionInput | null > | null,
  or?: Array< ModelProductConditionInput | null > | null,
  not?: ModelProductConditionInput | null,
};

export type ModelFloatInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type UpdateProductInput = {
  id: string,
  name?: string | null,
  image?: string | null,
  price?: number | null,
  slug?: string | null,
  productCategoryId?: string | null,
};

export type DeleteProductInput = {
  id?: string | null,
};

export type ModelHeroFilterInput = {
  id?: ModelIDInput | null,
  image?: ModelStringInput | null,
  active?: ModelBooleanInput | null,
  altImage?: ModelStringInput | null,
  page?: ModelStringInput | null,
  and?: Array< ModelHeroFilterInput | null > | null,
  or?: Array< ModelHeroFilterInput | null > | null,
  not?: ModelHeroFilterInput | null,
};

export type ModelIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export type ModelCategoryFilterInput = {
  id?: ModelIDInput | null,
  name?: ModelStringInput | null,
  image?: ModelStringInput | null,
  slug?: ModelStringInput | null,
  and?: Array< ModelCategoryFilterInput | null > | null,
  or?: Array< ModelCategoryFilterInput | null > | null,
  not?: ModelCategoryFilterInput | null,
};

export type ModelProductFilterInput = {
  id?: ModelIDInput | null,
  name?: ModelStringInput | null,
  image?: ModelStringInput | null,
  price?: ModelFloatInput | null,
  slug?: ModelStringInput | null,
  and?: Array< ModelProductFilterInput | null > | null,
  or?: Array< ModelProductFilterInput | null > | null,
  not?: ModelProductFilterInput | null,
};

export type CreateHeroMutationVariables = {
  input: CreateHeroInput,
  condition?: ModelHeroConditionInput | null,
};

export type CreateHeroMutation = {
  createHero:  {
    __typename: "Hero",
    id: string,
    image: string,
    active: boolean,
    altImage: string | null,
    page: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateHeroMutationVariables = {
  input: UpdateHeroInput,
  condition?: ModelHeroConditionInput | null,
};

export type UpdateHeroMutation = {
  updateHero:  {
    __typename: "Hero",
    id: string,
    image: string,
    active: boolean,
    altImage: string | null,
    page: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteHeroMutationVariables = {
  input: DeleteHeroInput,
  condition?: ModelHeroConditionInput | null,
};

export type DeleteHeroMutation = {
  deleteHero:  {
    __typename: "Hero",
    id: string,
    image: string,
    active: boolean,
    altImage: string | null,
    page: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateCategoryMutationVariables = {
  input: CreateCategoryInput,
  condition?: ModelCategoryConditionInput | null,
};

export type CreateCategoryMutation = {
  createCategory:  {
    __typename: "Category",
    id: string,
    name: string,
    image: string,
    slug: string,
    createdAt: string,
    updatedAt: string,
    products:  {
      __typename: "ModelProductConnection",
      items:  Array< {
        __typename: "Product",
        id: string,
        name: string,
        image: string,
        price: number,
        slug: string,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
  } | null,
};

export type UpdateCategoryMutationVariables = {
  input: UpdateCategoryInput,
  condition?: ModelCategoryConditionInput | null,
};

export type UpdateCategoryMutation = {
  updateCategory:  {
    __typename: "Category",
    id: string,
    name: string,
    image: string,
    slug: string,
    createdAt: string,
    updatedAt: string,
    products:  {
      __typename: "ModelProductConnection",
      items:  Array< {
        __typename: "Product",
        id: string,
        name: string,
        image: string,
        price: number,
        slug: string,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
  } | null,
};

export type DeleteCategoryMutationVariables = {
  input: DeleteCategoryInput,
  condition?: ModelCategoryConditionInput | null,
};

export type DeleteCategoryMutation = {
  deleteCategory:  {
    __typename: "Category",
    id: string,
    name: string,
    image: string,
    slug: string,
    createdAt: string,
    updatedAt: string,
    products:  {
      __typename: "ModelProductConnection",
      items:  Array< {
        __typename: "Product",
        id: string,
        name: string,
        image: string,
        price: number,
        slug: string,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
  } | null,
};

export type CreateProductMutationVariables = {
  input: CreateProductInput,
  condition?: ModelProductConditionInput | null,
};

export type CreateProductMutation = {
  createProduct:  {
    __typename: "Product",
    id: string,
    name: string,
    image: string,
    price: number,
    slug: string,
    createdAt: string,
    updatedAt: string,
    category:  {
      __typename: "Category",
      id: string,
      name: string,
      image: string,
      slug: string,
      createdAt: string,
      updatedAt: string,
      products:  {
        __typename: "ModelProductConnection",
        nextToken: string | null,
      } | null,
    } | null,
  } | null,
};

export type UpdateProductMutationVariables = {
  input: UpdateProductInput,
  condition?: ModelProductConditionInput | null,
};

export type UpdateProductMutation = {
  updateProduct:  {
    __typename: "Product",
    id: string,
    name: string,
    image: string,
    price: number,
    slug: string,
    createdAt: string,
    updatedAt: string,
    category:  {
      __typename: "Category",
      id: string,
      name: string,
      image: string,
      slug: string,
      createdAt: string,
      updatedAt: string,
      products:  {
        __typename: "ModelProductConnection",
        nextToken: string | null,
      } | null,
    } | null,
  } | null,
};

export type DeleteProductMutationVariables = {
  input: DeleteProductInput,
  condition?: ModelProductConditionInput | null,
};

export type DeleteProductMutation = {
  deleteProduct:  {
    __typename: "Product",
    id: string,
    name: string,
    image: string,
    price: number,
    slug: string,
    createdAt: string,
    updatedAt: string,
    category:  {
      __typename: "Category",
      id: string,
      name: string,
      image: string,
      slug: string,
      createdAt: string,
      updatedAt: string,
      products:  {
        __typename: "ModelProductConnection",
        nextToken: string | null,
      } | null,
    } | null,
  } | null,
};

export type GetHeroQueryVariables = {
  id: string,
};

export type GetHeroQuery = {
  getHero:  {
    __typename: "Hero",
    id: string,
    image: string,
    active: boolean,
    altImage: string | null,
    page: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListHerosQueryVariables = {
  filter?: ModelHeroFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListHerosQuery = {
  listHeros:  {
    __typename: "ModelHeroConnection",
    items:  Array< {
      __typename: "Hero",
      id: string,
      image: string,
      active: boolean,
      altImage: string | null,
      page: string | null,
      createdAt: string,
      updatedAt: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type ListCategorysQueryVariables = {
  filter?: ModelCategoryFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListCategorysQuery = {
  listCategorys:  {
    __typename: "ModelCategoryConnection",
    items:  Array< {
      __typename: "Category",
      id: string,
      name: string,
      image: string,
      slug: string,
      createdAt: string,
      updatedAt: string,
      products:  {
        __typename: "ModelProductConnection",
        nextToken: string | null,
      } | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetCategoryQueryVariables = {
  id: string,
};

export type GetCategoryQuery = {
  getCategory:  {
    __typename: "Category",
    id: string,
    name: string,
    image: string,
    slug: string,
    createdAt: string,
    updatedAt: string,
    products:  {
      __typename: "ModelProductConnection",
      items:  Array< {
        __typename: "Product",
        id: string,
        name: string,
        image: string,
        price: number,
        slug: string,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
  } | null,
};

export type GetProductQueryVariables = {
  id: string,
};

export type GetProductQuery = {
  getProduct:  {
    __typename: "Product",
    id: string,
    name: string,
    image: string,
    price: number,
    slug: string,
    createdAt: string,
    updatedAt: string,
    category:  {
      __typename: "Category",
      id: string,
      name: string,
      image: string,
      slug: string,
      createdAt: string,
      updatedAt: string,
      products:  {
        __typename: "ModelProductConnection",
        nextToken: string | null,
      } | null,
    } | null,
  } | null,
};

export type ListProductsQueryVariables = {
  filter?: ModelProductFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListProductsQuery = {
  listProducts:  {
    __typename: "ModelProductConnection",
    items:  Array< {
      __typename: "Product",
      id: string,
      name: string,
      image: string,
      price: number,
      slug: string,
      createdAt: string,
      updatedAt: string,
      category:  {
        __typename: "Category",
        id: string,
        name: string,
        image: string,
        slug: string,
        createdAt: string,
        updatedAt: string,
      } | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type OnCreateHeroSubscription = {
  onCreateHero:  {
    __typename: "Hero",
    id: string,
    image: string,
    active: boolean,
    altImage: string | null,
    page: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateHeroSubscription = {
  onUpdateHero:  {
    __typename: "Hero",
    id: string,
    image: string,
    active: boolean,
    altImage: string | null,
    page: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteHeroSubscription = {
  onDeleteHero:  {
    __typename: "Hero",
    id: string,
    image: string,
    active: boolean,
    altImage: string | null,
    page: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnCreateCategorySubscription = {
  onCreateCategory:  {
    __typename: "Category",
    id: string,
    name: string,
    image: string,
    slug: string,
    createdAt: string,
    updatedAt: string,
    products:  {
      __typename: "ModelProductConnection",
      items:  Array< {
        __typename: "Product",
        id: string,
        name: string,
        image: string,
        price: number,
        slug: string,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
  } | null,
};

export type OnUpdateCategorySubscription = {
  onUpdateCategory:  {
    __typename: "Category",
    id: string,
    name: string,
    image: string,
    slug: string,
    createdAt: string,
    updatedAt: string,
    products:  {
      __typename: "ModelProductConnection",
      items:  Array< {
        __typename: "Product",
        id: string,
        name: string,
        image: string,
        price: number,
        slug: string,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
  } | null,
};

export type OnDeleteCategorySubscription = {
  onDeleteCategory:  {
    __typename: "Category",
    id: string,
    name: string,
    image: string,
    slug: string,
    createdAt: string,
    updatedAt: string,
    products:  {
      __typename: "ModelProductConnection",
      items:  Array< {
        __typename: "Product",
        id: string,
        name: string,
        image: string,
        price: number,
        slug: string,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
  } | null,
};

export type OnCreateProductSubscription = {
  onCreateProduct:  {
    __typename: "Product",
    id: string,
    name: string,
    image: string,
    price: number,
    slug: string,
    createdAt: string,
    updatedAt: string,
    category:  {
      __typename: "Category",
      id: string,
      name: string,
      image: string,
      slug: string,
      createdAt: string,
      updatedAt: string,
      products:  {
        __typename: "ModelProductConnection",
        nextToken: string | null,
      } | null,
    } | null,
  } | null,
};

export type OnUpdateProductSubscription = {
  onUpdateProduct:  {
    __typename: "Product",
    id: string,
    name: string,
    image: string,
    price: number,
    slug: string,
    createdAt: string,
    updatedAt: string,
    category:  {
      __typename: "Category",
      id: string,
      name: string,
      image: string,
      slug: string,
      createdAt: string,
      updatedAt: string,
      products:  {
        __typename: "ModelProductConnection",
        nextToken: string | null,
      } | null,
    } | null,
  } | null,
};

export type OnDeleteProductSubscription = {
  onDeleteProduct:  {
    __typename: "Product",
    id: string,
    name: string,
    image: string,
    price: number,
    slug: string,
    createdAt: string,
    updatedAt: string,
    category:  {
      __typename: "Category",
      id: string,
      name: string,
      image: string,
      slug: string,
      createdAt: string,
      updatedAt: string,
      products:  {
        __typename: "ModelProductConnection",
        nextToken: string | null,
      } | null,
    } | null,
  } | null,
};
