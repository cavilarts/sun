import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import Navigation from './components/molecules/navigation/navigation'
import Home from './components/pages/home/home'
import Admin from './components/pages/admin/admin'
import CategoriesPage from './components/pages/categories/categories'
import ProductsPage from './components/pages/products/products'
import './App.scss'

function App() {
  return (
    <Router>
      <Navigation />
      <Switch>
        <Route component={Home} path="/" exact />
        <Route component={Admin} path="/admin" exact />
        <Route component={CategoriesPage} path="/categorias" exact />
        <Route component={ProductsPage} path="/productos" exact />
      </Switch>
    </Router>
  )
}

export default App
