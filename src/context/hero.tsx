import React, { useState, useEffect } from 'react'
import { API } from 'aws-amplify'

import { listHeros } from '../graphql/queries'

const HeroContext = React.createContext({
  hero: {},
  loading: false,
})

const HeroProvider = ({ children, page }: any) => {
  const [hero, setHero] = useState({})
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const fetchHero = async () => {
      try {
        setLoading(true)

        const { data }: any = await API.graphql({
          query: listHeros,
          filter: { page: { contains: page } },
          // @ts-ignore
          authMode: 'API_KEY',
        })

        setHero(data.listHeros.items[0])
        setLoading(false)
      } catch (error) {
        setLoading(false)
        console.log(error)
      }
    }
    fetchHero()
  }, [page])

  return <HeroContext.Provider value={{ hero, loading }}>{children}</HeroContext.Provider>
}

export { HeroContext, HeroProvider }
