import React, { useState, useEffect } from 'react'
import { API } from 'aws-amplify'

import { listCategorys } from '../graphql/queries'

const CategoriesContext = React.createContext({
  categories: [],
  loading: false,
})

const CategoriesProvider = ({ children }: any) => {
  const [categories, setCategories] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const fetchCategories = async () => {
      try {
        setLoading(true)

        const { data }: any = await API.graphql({
          query: listCategorys,
          // @ts-ignore
          authMode: 'API_KEY',
        })

        setCategories(data.listCategorys.items)
      } catch (error) {
        setLoading(false)
        console.log(error)
      }
    }
    fetchCategories()
  }, [])

  return (
    <CategoriesContext.Provider value={{ categories, loading }}>
      {children}
    </CategoriesContext.Provider>
  )
}

export { CategoriesContext, CategoriesProvider }
