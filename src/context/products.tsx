import React, { useState, useEffect } from 'react'
import { API } from 'aws-amplify'

import { listProducts } from '../graphql/queries'

const ProductsContext = React.createContext({
  products: [],
  loading: false,
})

const ProductsProvider = ({ children }: any) => {
  const [products, setProducts] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        setLoading(true)

        const { data }: any = await API.graphql({
          query: listProducts,
          // @ts-ignore
          authMode: 'API_KEY',
        })

        setProducts(data.listProducts.items)
      } catch (error) {
        setLoading(false)
        console.log(error)
      }
    }
    fetchProducts()
  }, [])

  return (
    <ProductsContext.Provider value={{ products, loading }}>{children}</ProductsContext.Provider>
  )
}

export { ProductsContext, ProductsProvider }
