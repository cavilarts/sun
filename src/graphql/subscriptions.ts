/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateHero = /* GraphQL */ `
  subscription OnCreateHero {
    onCreateHero {
      id
      image
      active
      altImage
      page
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateHero = /* GraphQL */ `
  subscription OnUpdateHero {
    onUpdateHero {
      id
      image
      active
      altImage
      page
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteHero = /* GraphQL */ `
  subscription OnDeleteHero {
    onDeleteHero {
      id
      image
      active
      altImage
      page
      createdAt
      updatedAt
    }
  }
`;
export const onCreateCategory = /* GraphQL */ `
  subscription OnCreateCategory {
    onCreateCategory {
      id
      name
      image
      slug
      createdAt
      updatedAt
      products {
        items {
          id
          name
          image
          price
          slug
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;
export const onUpdateCategory = /* GraphQL */ `
  subscription OnUpdateCategory {
    onUpdateCategory {
      id
      name
      image
      slug
      createdAt
      updatedAt
      products {
        items {
          id
          name
          image
          price
          slug
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;
export const onDeleteCategory = /* GraphQL */ `
  subscription OnDeleteCategory {
    onDeleteCategory {
      id
      name
      image
      slug
      createdAt
      updatedAt
      products {
        items {
          id
          name
          image
          price
          slug
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;
export const onCreateProduct = /* GraphQL */ `
  subscription OnCreateProduct {
    onCreateProduct {
      id
      name
      image
      price
      slug
      createdAt
      updatedAt
      category {
        id
        name
        image
        slug
        createdAt
        updatedAt
        products {
          nextToken
        }
      }
    }
  }
`;
export const onUpdateProduct = /* GraphQL */ `
  subscription OnUpdateProduct {
    onUpdateProduct {
      id
      name
      image
      price
      slug
      createdAt
      updatedAt
      category {
        id
        name
        image
        slug
        createdAt
        updatedAt
        products {
          nextToken
        }
      }
    }
  }
`;
export const onDeleteProduct = /* GraphQL */ `
  subscription OnDeleteProduct {
    onDeleteProduct {
      id
      name
      image
      price
      slug
      createdAt
      updatedAt
      category {
        id
        name
        image
        slug
        createdAt
        updatedAt
        products {
          nextToken
        }
      }
    }
  }
`;
