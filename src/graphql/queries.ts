/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getHero = /* GraphQL */ `
  query GetHero($id: ID!) {
    getHero(id: $id) {
      id
      image
      active
      altImage
      page
      createdAt
      updatedAt
    }
  }
`;
export const listHeros = /* GraphQL */ `
  query ListHeros(
    $filter: ModelHeroFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listHeros(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        image
        active
        altImage
        page
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const listCategorys = /* GraphQL */ `
  query ListCategorys(
    $filter: ModelCategoryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCategorys(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        image
        slug
        createdAt
        updatedAt
        products {
          nextToken
        }
      }
      nextToken
    }
  }
`;
export const getCategory = /* GraphQL */ `
  query GetCategory($id: ID!) {
    getCategory(id: $id) {
      id
      name
      image
      slug
      createdAt
      updatedAt
      products {
        items {
          id
          name
          image
          price
          slug
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;
export const getProduct = /* GraphQL */ `
  query GetProduct($id: ID!) {
    getProduct(id: $id) {
      id
      name
      image
      price
      slug
      createdAt
      updatedAt
      category {
        id
        name
        image
        slug
        createdAt
        updatedAt
        products {
          nextToken
        }
      }
    }
  }
`;
export const listProducts = /* GraphQL */ `
  query ListProducts(
    $filter: ModelProductFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listProducts(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        image
        price
        slug
        createdAt
        updatedAt
        category {
          id
          name
          image
          slug
          createdAt
          updatedAt
        }
      }
      nextToken
    }
  }
`;
