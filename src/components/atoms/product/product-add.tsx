import React, { useState, ReactNode, SyntheticEvent, useEffect } from 'react'
import { Storage, API, graphqlOperation } from 'aws-amplify'
import { v4 } from 'uuid'

import Icon from '../icon/icon'
import Overlay from '../overlay/overlay'
import config from '../../../aws-exports'
import { createProduct } from '../../../graphql/mutations'
import { listCategorys } from '../../../graphql/queries'

const ProductAdd: React.FC = () => {
  const [openOverlay, setOpenOverlay] = useState(false)

  return (
    <>
      <article>
        <h3>Add New Product</h3>
        <button
          onClick={() => {
            setOpenOverlay(true)
          }}
        >
          <Icon name="plus" size="large" />
        </button>
      </article>
      <Overlay
        isOpen={openOverlay}
        closeOverlay={() => {
          setOpenOverlay(false)
        }}
      >
        {RenderOverlayForm()}
      </Overlay>
    </>
  )
}

const RenderOverlayForm = (): ReactNode => {
  const [product, setProduct] = useState({
    name: '',
    image: '',
    price: 0,
    slug: '',
    productCategoryId: '',
  })
  const [image, setImage] = useState<any>(null)
  const [categories, setCategories] = useState<any>([])

  useEffect(() => {
    getCategories()
  }, [])

  const handleImageUpload = async (event: SyntheticEvent<HTMLInputElement>) => {
    event.preventDefault()

    const target = event.currentTarget
    const file = target.files![0]
    const extension = file.name.split('.')[1]
    const name = file.name.split('.')[0]
    const key = `images/${v4()}${name}.${extension}`
    const url = `https://${config.aws_user_files_s3_bucket}.s3.${config.aws_user_files_s3_bucket_region}.amazonaws.com/public/${key}`
    try {
      await Storage.put(key, file, {
        level: 'public',
        contentType: file.type,
      })

      const image = await Storage.get(key, { level: 'public' })
      setImage(image)
      setProduct({ ...product, image: url })
    } catch (err) {
      console.log(err)
    }
  }

  const handleSubmit = async (event: SyntheticEvent<HTMLButtonElement>) => {
    debugger
    event.preventDefault()

    try {
      if (
        !product.image ||
        !product.name ||
        !product.slug ||
        !product.price ||
        !product.productCategoryId
      )
        return

      await API.graphql(graphqlOperation(createProduct, { input: product }))
    } catch (error) {
      console.log(`error creating product`, error)
    }
  }

  const getCategories = async () => {
    try {
      const { data }: any = await API.graphql(graphqlOperation(listCategorys))
      setCategories(data.listCategorys.items)
    } catch (error) {
      console.log(`error creating product`, error)
    }
  }

  return (
    <section>
      <div>
        <label htmlFor="product-image">Imagen</label>
        {image ? <img src={image} alt="uploaded" /> : null}
        <input type="file" name="product-image" id="product-image" onChange={handleImageUpload} />
      </div>
      <div>
        <label htmlFor="product-name">Nombre del Producto</label>
        <input
          type="text"
          name="product-name"
          id="product-name"
          onChange={(evt) => {
            setProduct({ ...product, name: evt.target.value })
          }}
        />
      </div>
      <div>
        <label htmlFor="product-slug">Slug (este campo sirve para la url)</label>
        <input
          type="text"
          name="product-slug"
          id="product-slug"
          onChange={(evt) => {
            setProduct({ ...product, slug: evt.target.value })
          }}
        />
      </div>
      <div>
        <label htmlFor="product-category">Categoria</label>
        <select
          name="product-category"
          id="product-category"
          onClick={(evt) => {
            setProduct({ ...product, productCategoryId: evt.currentTarget.value })
          }}
          onChange={(evt) => {
            setProduct({ ...product, productCategoryId: evt.target.value })
          }}
          value={categories.length ? categories[0].id : ''}
        >
          {categories.map((c: any) => (
            <option value={c.id} key={c.id}>
              {c.name}
            </option>
          ))}
        </select>
      </div>
      <div>
        <label htmlFor="product-price">Precio</label>
        <input
          type="number"
          name="product-price"
          id="product-price"
          onChange={(evt) => {
            setProduct({ ...product, price: +evt.target.value })
          }}
        />
      </div>
      <button onClick={handleSubmit}>Guardar Producto</button>
    </section>
  )
}

export default ProductAdd
