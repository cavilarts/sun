import React from 'react'

import { ProductProps } from './product.interface'
import { Link } from 'react-router-dom'
import './product.scss'

const Product: React.FC<ProductProps> = ({ name, image, slug, price }) => {
  return (
    <article className="product">
      <Link to={`/products/${slug}`} className="product--link">
        <img src={image} alt={name}  className="product--image"/>
      </Link>
      <a href={`whatsapp://send?text=Hola,%20quisiera%20pedir%20${name}!&phone=+573017317313`} className="product--request">Pedir</a>
      <Link to={`/products/${slug}`} className="product--link">
        <p className="product--price">{price}</p>
        <h3 className="product--name">{name}</h3>
      </Link>
    </article>
  )
}

export default Product
