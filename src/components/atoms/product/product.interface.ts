export interface ProductProps {
  name: string
  image: string
  price: string
  slug: string
  category: any
}
