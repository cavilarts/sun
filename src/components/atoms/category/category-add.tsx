import React, { useState, ReactNode, SyntheticEvent } from 'react'
import { Storage, API, graphqlOperation } from 'aws-amplify'
import { v4 } from 'uuid'

import Icon from '../icon/icon'
import Overlay from '../overlay/overlay'
import config from '../../../aws-exports'
import { createCategory } from '../../../graphql/mutations'

const CategoryAdd: React.FC = () => {
  const [openOverlay, setOpenOverlay] = useState(false)

  return (
    <>
      <article>
        <h3>Add New Category</h3>
        <button
          onClick={() => {
            setOpenOverlay(true)
          }}
        >
          <Icon name="plus" size="large" />
        </button>
      </article>
      <Overlay
        isOpen={openOverlay}
        closeOverlay={() => {
          setOpenOverlay(false)
        }}
      >
        {RenderOverlayForm()}
      </Overlay>
    </>
  )
}

const RenderOverlayForm = (): ReactNode => {
  const [category, setCategory] = useState({ name: '', image: '', slug: '' })
  const [image, setImage] = useState<any>(null)

  const handleImageUpload = async (event: SyntheticEvent<HTMLInputElement>) => {
    event.preventDefault()

    const target = event.currentTarget
    const file = target.files![0]
    const extension = file.name.split('.')[1]
    const name = file.name.split('.')[0]
    const key = `images/${v4()}${name}.${extension}`
    const url = `https://${config.aws_user_files_s3_bucket}.s3.${config.aws_user_files_s3_bucket_region}.amazonaws.com/public/${key}`
    try {
      await Storage.put(key, file, {
        level: 'public',
        contentType: file.type,
      })

      const image = await Storage.get(key, { level: 'public' })
      setImage(image)
      setCategory({ ...category, image: url })
    } catch (err) {
      console.log(err)
    }
  }

  const handleSubmit = async (event: SyntheticEvent<HTMLButtonElement>) => {
    event.preventDefault()

    try {
      if (!category.image || !category.name || !category.slug) return

      await API.graphql(graphqlOperation(createCategory, { input: category }))
    } catch (error) {
      console.log(`error creating category`, error)
    }
  }

  return (
    <section>
      <div>
        <label htmlFor="category-image">Imagen</label>
        {image ? <img src={image} alt="uploaded" /> : null}
        <input type="file" name="category-image" id="category-image" onChange={handleImageUpload} />
      </div>
      <div>
        <label htmlFor="category-name">Nombre de la Categoria</label>
        <input
          type="text"
          name="category-name"
          id="category-name"
          onChange={(evt) => {
            setCategory({ ...category, name: evt.target.value })
          }}
        />
      </div>
      <div>
        <label htmlFor="category-slug">Slug (este campo sirve para la url)</label>
        <input
          type="text"
          name="category-slug"
          id="category-slug"
          onChange={(evt) => {
            setCategory({ ...category, slug: evt.target.value })
          }}
        />
      </div>
      <button onClick={handleSubmit}>Guardar Categoria</button>
    </section>
  )
}

export default CategoryAdd
