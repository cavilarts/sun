export interface CategoryProps {
  name: string
  image: string
  slug: string
}
