import React from 'react'
import { Link } from 'react-router-dom'

import { CategoryProps } from './category.interface'
import './category.scss'

const Category: React.FC<CategoryProps> = ({ name, image, slug }) => {
  return (
    <article className="category">
      <Link to={`/categories/${slug}`}>
        <img src={image} alt={name} className="category--image"/>
        <h3 className="category--name">{name}</h3>
      </Link>
    </article>
  )
}

export default Category
