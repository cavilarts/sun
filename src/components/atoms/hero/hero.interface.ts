import { ReactNode } from 'react'

export interface HeroProps {
  children?: ReactNode
}

export interface HeroAddProps {
  page: string
}
