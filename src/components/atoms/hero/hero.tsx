import React, { useContext } from 'react'

import { HeroProps } from './hero.interface'
import { HeroContext } from '../../../context/hero'
import './hero.scss'

const Hero: React.FC<HeroProps> = ({ children }) => {
  const { hero }: any = useContext(HeroContext)
  const heroImageStyle = {
    backgroundImage: hero ? `url(${hero.image})` : '',
  }

  return (
    <section className="hero">
      <div className="hero--image" style={heroImageStyle}></div>
      {children}
    </section>
  )
}

export default Hero
