import React, { useState, SyntheticEvent, useContext } from 'react'
import { v4 } from 'uuid'
import { Storage, API, graphqlOperation } from 'aws-amplify'

import config from '../../../aws-exports'
import Icon from '../icon/icon'
import { HeroAddProps } from './hero.interface'
import { createHero, deleteHero } from '../../../graphql/mutations'
import { HeroContext } from '../../../context/hero'
import './hero-add.scss'

const HeroAdd: React.FC<HeroAddProps> = ({ page }) => {
  const [image, setImage] = useState<any>(null)
  const [heroDetails, setHeroDetails] = useState({
    image: '',
    altImage: '',
    page: page,
    active: true,
  })
  const { hero }: any = useContext(HeroContext)

  const handleImageUpload = async (event: SyntheticEvent<HTMLInputElement>) => {
    event.preventDefault()

    const target = event.currentTarget
    const file = target.files![0]
    const extension = file.name.split('.')[1]
    const name = file.name.split('.')[0]
    const key = `images/${v4()}${name}.${extension}`
    const url = `https://${config.aws_user_files_s3_bucket}.s3.${config.aws_user_files_s3_bucket_region}.amazonaws.com/public/${key}`
    try {
      await Storage.put(key, file, {
        level: 'public',
        contentType: file.type,
      })

      const image = await Storage.get(key, { level: 'public' })
      setImage(image)
      setHeroDetails({ ...heroDetails, image: url })
    } catch (err) {
      console.log(err)
    }
  }

  const handleSubmit = async (event: SyntheticEvent<HTMLButtonElement>) => {
    event.preventDefault()

    try {
      if (!heroDetails.image || !heroDetails.page) return
      if (hero.image) {
        await API.graphql(graphqlOperation(deleteHero, { input: { id: hero.id } }))
      }
      await API.graphql(graphqlOperation(createHero, { input: heroDetails }))
    } catch (error) {
      console.log(`error creating hero`, error)
    }
  }

  return (
    <section className="hero-add">
      <div className="hero-add--image">
        {image ? <img src={image} alt={`hero-${page}`} /> : <Icon name="image" size="huge" />}
      </div>
      <div className="hero-add--actions">
        <input type="file" name="hero-upload" id="hero" onChange={handleImageUpload} />
        <button onClick={handleSubmit}>Guardar Hero</button>
      </div>
    </section>
  )
}

export default HeroAdd
