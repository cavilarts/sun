import React from 'react'
import classNames from 'classnames'

import { IconProps } from './icon.interface'
import './icon.scss'

const Icon: React.FC<IconProps> = ({ name, size, className }) => {
  const iconClassName = classNames(`icon-${name}`, `icon__${size}`, className)

  return <span className={iconClassName}></span>
}

export default Icon
