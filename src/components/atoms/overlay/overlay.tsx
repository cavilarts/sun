import React, { useCallback } from 'react'
import classNames from 'classnames'

import Icon from '../icon/icon'
import { OverlayProps } from './overlay.interface'
import './overlay.scss'

const Overlay: React.FC<OverlayProps> = ({ children, isOpen, closeOverlay }) => {
  const overlayClassName = classNames('overlay', { open: isOpen })
  const callbackCloseOverlay = useCallback(() => {
    closeOverlay()
  }, [closeOverlay])

  return (
    <section className={overlayClassName}>
      <div className="overlay--mask"></div>
      <div className="overlay--content-container">
        <div className="overlay--content-header">
          <button onClick={callbackCloseOverlay}>
            <Icon name="cross" size="medium" />
          </button>
        </div>
        <div className="overlay--content">{children}</div>
      </div>
    </section>
  )
}

export default Overlay
