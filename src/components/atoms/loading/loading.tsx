import React from 'react'
import classNames from 'classnames'

import Icon from '../icon/icon'
import { LoadingProps } from './loading.interface'
import './loading.scss'

const Loading: React.FC<LoadingProps> = ({ active }) => {
  const loadingClassName = classNames('loading', { active: active })

  return (
    <section className={loadingClassName}>
      <Icon name="spinner" size="huge" className="loading--spinner" />
    </section>
  )
}

export default Loading
