import React from 'react'

import './separator.scss'

const Separator = () => {
  return <div className="separator"></div>
}

export default Separator
