import React, { useState } from 'react'

import Hero from '../../atoms/hero/hero'
import Categories from '../../molecules/categories/categories'
import Products from '../../molecules/products/products'
import Loading from '../../atoms/loading/loading'
import Separator from '../../atoms/separator/separator'
import { HeroProvider } from '../../../context/hero'
import { CategoriesProvider } from '../../../context/categories'
import { ProductsProvider } from '../../../context/products'

const Home = () => {
  const [loading, setLoading] = useState(false)

  return (
    <section>
      <Loading active={loading} />
      <HeroProvider page="home">
        <Hero />
      </HeroProvider>
      <CategoriesProvider>
        <Categories setLoading={setLoading} />
      </CategoriesProvider>
      <Separator />
      <ProductsProvider>
        <Products setLoading={setLoading} />
      </ProductsProvider>
    </section>
  )
}

export default Home
