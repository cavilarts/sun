import React, { useState } from 'react'

import Categories from '../../molecules/categories/categories'
import { CategoriesProvider } from '../../../context/categories'
import Loading from '../../atoms/loading/loading'

const CategoriesPage = () => {
  const [loading, setLoading] = useState(false)
  return (
    <>
      <Loading active={loading} />
      <CategoriesProvider>
        <h1>Categorias</h1>
        <Categories setLoading={setLoading} />
      </CategoriesProvider>
    </>
  )
}

export default CategoriesPage
