import React, { useState } from 'react'
import { AmplifyAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react'

import Hero from '../../atoms/hero/hero'
import HeroAdd from '../../atoms/hero/hero-add'
import Overlay from '../../atoms/overlay/overlay'
import Categories from '../../molecules/categories/categories'
import CategoryAdd from '../../atoms/category/category-add'
import Products from '../../molecules/products/products'
import ProductAdd from '../../atoms/product/product-add'
import { HeroProvider } from '../../../context/hero'
import { CategoriesProvider } from '../../../context/categories'
import { ProductsProvider } from '../../../context/products'
import Loading from '../../atoms/loading/loading'

const AdminPage: React.FC = () => {
  const page = 'home'
  const [overlayOpen, setOverlayOpen] = useState(false)
  const [loading, setLoading] = useState(false)

  return (
    <section>
      <Loading active={loading} />
      <AmplifyAuthenticator>
        <AmplifySignOut></AmplifySignOut>
        <HeroProvider page={page}>
          <Hero />
          <button
            onClick={() => {
              setOverlayOpen(true)
            }}
          >
            Edit Hero
          </button>
          <Overlay
            isOpen={overlayOpen}
            closeOverlay={() => {
              setOverlayOpen(false)
            }}
          >
            <HeroAdd page={page} />
          </Overlay>
        </HeroProvider>
        <CategoriesProvider>
          <h2>Categorias</h2>
          <Categories setLoading={setLoading} />
          <CategoryAdd />
        </CategoriesProvider>
        <ProductsProvider>
          <h2>Productos</h2>
          <Products setLoading={setLoading} />
          <ProductAdd />
        </ProductsProvider>
      </AmplifyAuthenticator>
    </section>
  )
}

export default AdminPage
