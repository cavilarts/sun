import React, { useState } from 'react'

import Products from '../../molecules/products/products'
import { ProductsProvider } from '../../../context/products'
import Loading from '../../atoms/loading/loading'

const ProductsPage = () => {
  const [loading, setLoading] = useState(false)

  return (
    <>
      <Loading active={loading} />
      <ProductsProvider>
        <h1>Productos</h1>
        <Products setLoading={setLoading} />
      </ProductsProvider>
    </>
  )
}

export default ProductsPage
