import React, { useContext, useEffect } from 'react'

import Product from '../../atoms/product/product'
import { ProductsContextInterface, ProductInterface, ProductProps } from './products.interface'
import { ProductsContext } from '../../../context/products'
import './products.scss'

const Products: React.FC<ProductProps> = ({ setLoading }) => {
  const { products, loading }: ProductsContextInterface = useContext(ProductsContext)

  useEffect(() => {
    setLoading(loading)
  }, [setLoading, loading])

  return (
    <>
      <h2 className="products--title">Nuestros Tejidos</h2>
      <section className="products">
        {loading}
        {products.map((product: ProductInterface) => (
          <Product
            name={product.name}
            image={product.image}
            slug={product.slug}
            price={product.price}
            category={product.category}
            key={product.id}
          />
        ))}
      </section>
    </>
  )
}

export default Products
