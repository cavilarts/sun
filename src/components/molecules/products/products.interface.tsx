export interface ProductsContextInterface {
  products: ProductInterface[]
  loading: boolean
}

export interface ProductInterface {
  id: string
  name: string
  image: string
  price: string
  slug: string
  category: string
}

export interface ProductProps {
  setLoading: (loading: boolean) => void
}
