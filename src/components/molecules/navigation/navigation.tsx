import React from 'react'
import { NavLink, Link } from 'react-router-dom'

import './navigation.scss'

const Navigation = () => {
  return (
    <nav className="navigation">
      <div className="navigation--logo">
        <Link to="/">
          <img src="/logo.png" alt="logo" />
        </Link>
      </div>
      <div className="navigation--links">
        <NavLink
          to="/categorias"
          activeClassName="navigation--link__active"
          className="navigation--link"
        >
          Categorias
        </NavLink>
        <NavLink
          to="/productos"
          activeClassName="navigation--link__active"
          className="navigation--link"
        >
          Productos
        </NavLink>
      </div>
    </nav>
  )
}

export default Navigation
