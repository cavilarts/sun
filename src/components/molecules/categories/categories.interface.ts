export interface CategoriesContextInterface {
  categories: CategoryInterface[]
  loading: boolean
}

export interface CategoryInterface {
  id: string
  name: string
  image: string
  slug: string
  products: string
}

export interface CategoriesProps {
  setLoading: (loading: boolean) => void
}
