import React, { useContext, useEffect } from 'react'

import Category from '../../atoms/category/category'
import {
  CategoriesContextInterface,
  CategoryInterface,
  CategoriesProps,
} from './categories.interface'
import { CategoriesContext } from '../../../context/categories'
import './categories.scss'

const Categories: React.FC<CategoriesProps> = ({ setLoading }) => {
  const { categories, loading }: CategoriesContextInterface = useContext(CategoriesContext)

  useEffect(() => {
    setLoading(loading)
  }, [setLoading, loading])

  return (
    <>
      <section className="categories">
        {categories.map((c: CategoryInterface) => (
          <Category name={c.name} image={c.image} slug={c.slug} key={c.id} />
        ))}
      </section>
    </>
  )
}

export default Categories
